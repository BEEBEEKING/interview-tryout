# interview-tryout

This is a microservice that connects to Postgres to store messages and whether or not they are a palindrone. The database is initialized on Postgres startup with the init.sql file. Once Postgres has started up properly, the microservice will then start up and try to connect to Postgres. Once it connects, it will wait for users to request data from it.

You can interact with the microservice REST API to create, retrieve, update, and delete messages. You can also list all the messages that are being stored with their corresponding numeric id and whether or not they are a palindrone.

For more details on the REST API and how to interact with it, please refer to the swagger.yml file. 
You can copy the file and paste it into [this website](http://editor.swagger.io) to view it better

# How to build and run:
1) Have Docker, Docker-Compose, Go and Go Dep installed.

[Install Docker-Compose by following the instructions here](https://docs.docker.com/compose/install/#install-compose)

[Install Go](https://golang.org/doc/install)

[Install Go Dep](https://github.com/golang/dep#installation)

2) Run the build.sh script included inside the folder - this should create the docker image necessary.

3) Run the Docker-Compose file with the command: 
``` docker-compose up -d ```

First time users will download a Postgresql image from the Public Docker Repository. Then, the Docker Network should start up a Postgres container and then the REST API.
You should be able to query the API from this point. 

# How to test:
Note: You need Docker and Docker-Compose installed to run the tests.

Run the test.sh script included in this directory. This will boot up a Docker network with a Postgres DB and some of the columns inside of it. This uses an alternate
Docker-Compose file that will preload the database in order to test functionality. This network exposes the Postgres port on localhost:5432. The microservice will then run its tests and connect through the exposed port on localhost. 
