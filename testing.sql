CREATE DATABASE messagesdb;
CREATE USER rest WITH PASSWORD 'rest';
GRANT ALL PRIVILEGES ON DATABASE "messagesdb" to rest;

\c messagesdb

CREATE TABLE IF NOT EXISTS messages (
		id SERIAL PRIMARY KEY,
		message TEXT,
		is_palindrone bool
);
ALTER TABLE messages OWNER TO rest;
INSERT INTO messages (message, is_palindrone) VALUES ('hey this is a message', false), ('this is for testing', false), ('kayak', true), ('fake message', false);