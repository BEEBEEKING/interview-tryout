package model

// Struct that is used to marshal json request body
// These also correspond with the columns inside the database table.
type PalinMessage struct {
	Id           int    `json:"id"`
	Message      string `json:"message"`
	IsPalindrone bool   `json:"is_palindrone"`
}
