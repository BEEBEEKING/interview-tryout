#!/bin/sh

dep ensure -v
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o interview .
docker build -t interview -f Dockerfile .