package controller

import (
	"database/sql"
	"encoding/json"
	"net/http"

	"interview-tryout/db"
	"interview-tryout/model"
)

// Handler given to each endpoint that needs a connection with the database
type DatabaseHandler struct {
	DBConn *sql.DB
}

// reverse takes a string and returns the string in reverse.
// Goes through a loop and swaps the first and last character of the string and then moves inward to swap again.
func reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

// Handler function for /v1/messages
func (dh *DatabaseHandler) HandleMessages(w http.ResponseWriter, r *http.Request) {
	var record model.PalinMessage
	if r.Body == nil {
		http.Error(w, "Please send a request body", http.StatusBadRequest)
		return
	}

	err := json.NewDecoder(r.Body).Decode(&record)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	switch r.Method {
	case http.MethodGet:
		// Serve the resource.
		if record.Id <= 0 {
			http.Error(w, "Please select a positive ID to retrieve", http.StatusBadRequest)
			return
		}
		row, err := db.RetrieveMessage(dh.DBConn, record.Id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		json, err := json.Marshal(row)
		if err != nil {
			panic(err)
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(json)
		return
	case http.MethodPost:
		// Create a new record.
		if record.Message == reverse(record.Message) {
			record.IsPalindrone = true
		}
		id, err := db.AddMessage(dh.DBConn, record.Message, record.IsPalindrone)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		record.Id = id
		json, err := json.Marshal(record)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		w.Write(json)
		return
	case http.MethodPut:
		// Update an existing record.
		if record.Id <= 0 {
			http.Error(w, "Please select a positive ID to update", http.StatusBadRequest)
			return
		}
		if record.Message == reverse(record.Message) {
			record.IsPalindrone = true
		}
		_, err := db.UpdateMessage(dh.DBConn, record)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusNoContent)
		return
	case http.MethodDelete:
		// Remove the record.
		if record.Id <= 0 {
			http.Error(w, "Please select an ID to delete", http.StatusBadRequest)
			return
		}
		_, err := db.DeleteMessage(dh.DBConn, record.Id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusNoContent)
		return
	default:
		// Throws an error since the client is requesting a method not supported.
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return

	}

}

// Handler function for /v1/list
func (dh *DatabaseHandler) HandleList(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	records := []model.PalinMessage{}

	rows, err := db.ListTable(dh.DBConn)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	for rows.Next() {
		record := model.PalinMessage{}
		err = rows.Scan(&record.Id, &record.Message, &record.IsPalindrone)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		records = append(records, record)
	}
	err = rows.Err()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json, err := json.Marshal(records)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(json)
	return

}
