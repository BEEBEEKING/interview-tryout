package db

import (
	"database/sql"
	"errors"
	"fmt"
	"interview-tryout/model"
	"log"
	"os"

	"time"

	_ "github.com/lib/pq"
)

var (
	host     = "127.0.0.1"
	port     = 5432
	user     = "rest"
	password = "rest"
	dbname   = "messagesdb"
)

//SecureConnnection tries to set up a connection with Postgres and will attempt to connect to it
//If Postgres is available, it will return the connection
func SecureConnection() *sql.DB {
	// Checks an env variable to overwrite where to look for the host
	if os.Getenv("POSTGRES_HOST") != "" {
		host = os.Getenv("POSTGRES_HOST")
	}
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	log.Println("Checking if Postgres is ready")

	for i := 0; i > 10; i++ {
		err = db.Ping()
		if err == nil {
			break
		}
		if err != nil && i >= 10 {
			panic(err)
		}
		log.Printf(".")
		time.Sleep(5 * time.Second)
	}

	log.Println("Successfully connected!")

	return db
}

//AddMessage takes a DB connection, the message that was sent and if it is a palidrone. It attempts to insert the record into the database
//Returns the numeric ID of the record inside the DB if it was correctly inserted
func AddMessage(database *sql.DB, message string, isPalindrone bool) (int, error) {
	id := 0
	err := database.QueryRow(`INSERT INTO messages (message, is_palindrone) VALUES ($1, $2) RETURNING id`, message, isPalindrone).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil
}

//UpdateMessage takes a DB connection and the message details (id, message string and palindrone boolean)
//Returns an SQL result if the command succeeded
func UpdateMessage(database *sql.DB, record model.PalinMessage) (sql.Result, error) {
	res, err := database.Exec(`UPDATE messages SET message = $2, is_palindrone = $3 WHERE id = $1`,
		record.Id, record.Message, record.IsPalindrone)
	if err != nil {
		return nil, err
	}
	return res, nil
}

//UpdateMessage takes a DB connection and the message id that is to be retrieved
//Returns the full record inside the DB if it was correctly retrieved
func RetrieveMessage(database *sql.DB, id int) (model.PalinMessage, error) {
	record := model.PalinMessage{}
	row := database.QueryRow(`SELECT * FROM messages WHERE id = $1`, id)
	err := row.Scan(&record.Id, &record.Message, &record.IsPalindrone)
	switch err {
	case sql.ErrNoRows:
		return record, errors.New("Could not find record with that ID")
	case nil:
		return record, nil
	default:
		return record, err
	}
}

//DeleteMessage takes a DB connection and the message id to be deleted
//Returns an SQL result if the command succeeded
func DeleteMessage(database *sql.DB, id int) (sql.Result, error) {
	res, err := database.Exec(`DELETE FROM messages WHERE id = $1`, id)
	if err != nil {
		return nil, err
	}
	return res, nil
}

//ListTable takes a DB connection
//Returns a list of rows that were listed in the DB table
func ListTable(database *sql.DB) (*sql.Rows, error) {
	rows, err := database.Query(`SELECT * FROM messages;`)
	if err != nil {
		return nil, err
	}
	return rows, nil
}
