CREATE DATABASE messagesdb;
CREATE USER rest WITH PASSWORD 'rest';
GRANT ALL PRIVILEGES ON DATABASE "messagesdb" to rest;

\c messagesdb

CREATE TABLE IF NOT EXISTS messages (
		id SERIAL PRIMARY KEY,
		message TEXT,
		is_palindrone bool
);

ALTER TABLE messages OWNER TO rest;