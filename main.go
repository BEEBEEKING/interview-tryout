package main

import (
	"interview-tryout/controller"
	"interview-tryout/db"
	"log"
	"net/http"
)

func main() {

	database := db.SecureConnection()
	defer database.Close()

	dbhandler := &controller.DatabaseHandler{DBConn: database}
	http.HandleFunc("/v1/messages", dbhandler.HandleMessages)
	http.HandleFunc("/v1/list", dbhandler.HandleList)

	log.Fatal(http.ListenAndServe(":8080", nil))

}
