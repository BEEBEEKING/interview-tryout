package db

import (
	"database/sql"
	"interview-tryout/model"
	"testing"
)

func compare(expected, actual model.PalinMessage) bool {
	if actual.Id != expected.Id ||
		actual.Message != expected.Message ||
		actual.IsPalindrone != expected.IsPalindrone {
		return false
	} else {
		return true
	}
}

func Test_ListTable(t *testing.T) {
	db := SecureConnection()
	records := []model.PalinMessage{}

	rows, err := ListTable(db)
	for rows.Next() {
		record := model.PalinMessage{}
		err = rows.Scan(&record.Id, &record.Message, &record.IsPalindrone)
		if err != nil {
			panic(err)
		}
		records = append(records, record)
	}
	if len(records) <= 0 {
		t.Errorf("No records returned, length was %v", len(records))
	}

	actual := model.PalinMessage{
		Id:           3,
		Message:      "kayak",
		IsPalindrone: true,
	}
	if !compare(records[2], actual) {
		t.Errorf("Result was not the same: Have %v , want %v ", records[2], actual)
	}
}

func Test_AddMessage(t *testing.T) {
	db := SecureConnection()

	record := model.PalinMessage{
		Message:      "I am testing AddMessage function",
		IsPalindrone: false,
	}
	id, err := AddMessage(db, record.Message, record.IsPalindrone)
	if err != nil {
		panic(err)
	}
	record.Id = id
	actual := model.PalinMessage{}
	row := db.QueryRow(`SELECT * FROM messages WHERE id = $1`, id)
	err = row.Scan(&actual.Id, &actual.Message, &actual.IsPalindrone)
	if err != nil {
		panic(err)
	}
	if !compare(record, actual) {
		t.Errorf("Result was not the same: Have %v , want %v ", record, actual)
	}

}

func Test_RetrieveMessage(t *testing.T) {
	db := SecureConnection()

	expected := model.PalinMessage{
		Message:      "I am testing RetrieveMessage function",
		IsPalindrone: false,
	}
	id := 0
	err := db.QueryRow(`INSERT INTO messages (message, is_palindrone) VALUES ($1, $2) RETURNING id`, expected.Message, expected.IsPalindrone).Scan(&id)
	if err != nil {
		panic(err)
	}
	expected.Id = id
	actual, err := RetrieveMessage(db, expected.Id)
	if err != nil {
		panic(err)
	}
	if !compare(expected, actual) {
		t.Errorf("Result was not the same: Have %v , want %v ", expected, actual)
	}

}

func Test_DeleteMessage(t *testing.T) {
	db := SecureConnection()

	record := model.PalinMessage{
		Message:      "I am testing DeleteMessage function",
		IsPalindrone: false,
	}

	id := 0
	err := db.QueryRow(`INSERT INTO messages (message, is_palindrone) VALUES ($1, $2) RETURNING id`, record.Message, record.IsPalindrone).Scan(&id)
	if err != nil {
		panic(err)
	}

	res, err := DeleteMessage(db, id)
	if err != nil {
		panic(err)
	}
	numrows, err := res.RowsAffected()
	if err != nil {
		panic(err)
	}
	if numrows != 1 {
		t.Errorf("Result was not deleted, affected %v rows, expected 1", numrows)
	}
	actual := model.PalinMessage{}
	row := db.QueryRow(`SELECT * FROM messages WHERE id = $1`, id)
	err = row.Scan(&actual.Id, &actual.Message, &actual.IsPalindrone)
	if err != sql.ErrNoRows {
		t.Errorf("Error looking for deleted message: value found: %v error returned looking for value %v", actual, err)
	}
}

func Test_UpdateMessage(t *testing.T) {
	db := SecureConnection()

	record := model.PalinMessage{
		Message:      "I am testing UpdateMessage function",
		IsPalindrone: false,
	}

	id := 0
	err := db.QueryRow(`INSERT INTO messages (message, is_palindrone) VALUES ($1, $2) RETURNING id`, record.Message, record.IsPalindrone).Scan(&id)
	if err != nil {
		panic(err)
	}

	expected := model.PalinMessage{
		Id:           id,
		Message:      "I have changed this message",
		IsPalindrone: false,
	}
	res, err := UpdateMessage(db, expected)
	if err != nil {
		panic(err)
	}
	numrows, err := res.RowsAffected()
	if err != nil {
		panic(err)
	}
	if numrows != 1 {
		t.Errorf("Result was not updated, affected %v rows, expected 1", numrows)
	}
	actual := model.PalinMessage{}
	row := db.QueryRow(`SELECT * FROM messages WHERE id = $1`, id)
	err = row.Scan(&actual.Id, &actual.Message, &actual.IsPalindrone)
	if err != nil {
		t.Errorf("Error looking for deleted message: value found: %v error returned looking for value %v", actual, err)
	}

	if !compare(expected, actual) {
		t.Errorf("Result was not the same: Have %v , want %v ", expected, actual)
	}
}
