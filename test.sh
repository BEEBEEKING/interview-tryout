#!/bin/sh

docker-compose -f ./docker-compose-testing.yml up -d

while ! docker exec interview-tryout_test-postgres_1 pg_isready
do 
    echo "waiting for Postgres to start..."
    sleep 10
done

dep ensure -v 
go test ./controller ./db

docker-compose -f ./docker-compose-testing.yml down
