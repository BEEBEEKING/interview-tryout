package controller

import (
	"bytes"
	"encoding/json"
	"interview-tryout/db"
	"interview-tryout/model"
	"net/http"
	"net/http/httptest"
	"testing"
)

type Test struct {
	in  string
	out string
}

var tests = []Test{
	{"apple", "elppa"},
	{"banana", "ananab"},
	{"this is backwards", "sdrawkcab si siht"},
	{"kayak", "kayak"},
}

func TestReverse(t *testing.T) {
	for i, test := range tests {
		rev := reverse(test.in)
		if rev != test.out {
			t.Errorf("#%d: reverse(%s)=%s; want %s", i, test.in, rev, test.out)
		}
	}
}

func TestMessageGetRequest(t *testing.T) {
	// set up connection settings
	database := db.SecureConnection()

	// set up HTTP handler
	dbhandler := &DatabaseHandler{DBConn: database}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(dbhandler.HandleMessages)
	// Good request
	json := []byte(`{"id": 2}`)
	r := bytes.NewReader(json)
	req, err := http.NewRequest("GET", "/v1/messages", r)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
	expected := `{"id":2,"message":"this is for testing","is_palindrone":false}`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}

	// Bad request - negative number

	json = []byte(`{"id":-1}`)
	r = bytes.NewReader(json)
	req, err = http.NewRequest("GET", "/v1/messages", r)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusBadRequest)
	}

	// Can't retrieve number - id does not exist in DB

	json = []byte(`{"id":99}`)
	r = bytes.NewReader(json)
	req, err = http.NewRequest("GET", "/v1/messages", r)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusInternalServerError)
	}
}

func TestMessagePutRequest(t *testing.T) {
	// set up connection settings
	database := db.SecureConnection()

	// set up HTTP handler
	dbhandler := &DatabaseHandler{DBConn: database}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(dbhandler.HandleMessages)
	// Good request
	j := []byte(`{"id": 1, "message":"this was changed for a test"}`)
	r := bytes.NewReader(j)
	req, err := http.NewRequest("PUT", "/v1/messages", r)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusNoContent)
	}

	// Good request - validate isPalidrone is set on update
	j = []byte(`{"id": 2, "message":"racecar"}`)
	r = bytes.NewReader(j)
	req, err = http.NewRequest("PUT", "/v1/messages", r)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusNoContent)
	}

	j = []byte(`{"id": 2}`)
	r = bytes.NewReader(j)
	req, err = http.NewRequest("GET", "/v1/messages", r)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
	expected := model.PalinMessage{
		Id:           2,
		Message:      "racecar",
		IsPalindrone: true,
	}
	actual := model.PalinMessage{}

	err = json.Unmarshal(rr.Body.Bytes(), &actual)
	if err != nil {
		t.Fatal(err)
	}

	if expected != actual {
		t.Errorf("Objects are not the same: expected %v , actual %v", expected, actual)
	}

	// Bad request

	j = []byte(`{"id": -1}`)
	r = bytes.NewReader(j)
	req, err = http.NewRequest("PUT", "/v1/messages", r)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusBadRequest)
	}

	// Request does not have the id

	j = []byte(`{}`)
	r = bytes.NewReader(j)
	req, err = http.NewRequest("PUT", "/v1/messages", r)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusBadRequest)
	}

	// Request has an id not inside the database

	j = []byte(`{"id":99}`)
	r = bytes.NewReader(j)
	req, err = http.NewRequest("PUT", "/v1/messages", r)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusNoContent)
	}

}

func TestMessagePostRequest(t *testing.T) {
	// set up connection settings
	database := db.SecureConnection()

	// set up HTTP handler
	dbhandler := &DatabaseHandler{DBConn: database}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(dbhandler.HandleMessages)
	// Good request
	j := []byte(`{"message":"this message was posted through a test"}`)
	r := bytes.NewReader(j)
	req, err := http.NewRequest("POST", "/v1/messages", r)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusCreated)
	}
	expected := model.PalinMessage{
		Message:      "this message was posted through a test",
		IsPalindrone: false,
	}
	actual := model.PalinMessage{}
	err = json.Unmarshal(rr.Body.Bytes(), &actual)
	if err != nil {
		t.Fatal(err)
	}

	if expected.IsPalindrone != actual.IsPalindrone ||
		expected.Message != actual.Message {
		t.Errorf("Values do not equal! expected: %v actual: %v ", expected, actual)
	}

	// Good request with palindrone
	j = []byte(`{"message":"racecar"}`)
	r = bytes.NewReader(j)
	req, err = http.NewRequest("POST", "/v1/messages", r)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusCreated)
	}
	expected = model.PalinMessage{
		Message:      "racecar",
		IsPalindrone: true,
	}
	actual = model.PalinMessage{}
	err = json.Unmarshal(rr.Body.Bytes(), &actual)
	if err != nil {
		t.Fatal(err)
	}

	if expected.IsPalindrone != actual.IsPalindrone ||
		expected.Message != actual.Message {
		t.Errorf("Values do not equal! expected: %v actual: %v ", expected, actual)
	}

}

func TestMessageDeleteRequest(t *testing.T) {
	// set up connection settings
	database := db.SecureConnection()

	// set up HTTP handler
	dbhandler := &DatabaseHandler{DBConn: database}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(dbhandler.HandleMessages)
	// Good request
	json := []byte(`{"id":4}`)
	r := bytes.NewReader(json)
	req, err := http.NewRequest("DELETE", "/v1/messages", r)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusNoContent)
	}

	// deleting a record that isn't stored
	json = []byte(`{"id":99}`)
	r = bytes.NewReader(json)
	req, err = http.NewRequest("DELETE", "/v1/messages", r)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusNoContent)
	}

}

func TestListGetRequest(t *testing.T) {
	// set up connection settings
	database := db.SecureConnection()

	// set up HTTP handler
	dbhandler := &DatabaseHandler{DBConn: database}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(dbhandler.HandleList)
	// Good request
	j := []byte(`{}`)
	r := bytes.NewReader(j)
	req, err := http.NewRequest("GET", "/v1/list", r)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	records := []model.PalinMessage{}

	err = json.Unmarshal(rr.Body.Bytes(), &records)
	if err != nil {
		t.Fatal(err)
	}
	if len(records) < 1 {
		t.Errorf("No records found in Database. Found %v:", records)
	}
}
